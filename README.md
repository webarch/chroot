# Webarchitects Debian chroot Ansible role

[![pipeline status](https://git.coop/webarch/chroot/badges/master/pipeline.svg)](https://git.coop/webarch/chroot/-/commits/master)

An Ansible role to create a chroot on Debian Bullseye and Bookworm.

This repo does an initial chroot install on a `/chroot` partition and then clones the [chroot-config repo](https://git.coop/webarch/chroot-config) to `/root/chroot-config` and runs the Ansible in that repo using the Ansible chroot connection.

The directory for the chroot, `/chroot`, doesn't need to be a partition but you can lock it down more if it is, using things like `nosuid` when mounting it.

See the [defaults file](defaults/main.yml) for various options, set the `chroot` variable to `true` for the tasks in this role to be run, it defaults to `false`.

The way that Webarchitects uses chroots for shared web server is to mount them in an additional directory, read-only, for chrooted users.

Apache, PHP and SSH can be chrooted, in the case of PHP and SSH with a seperate chroot per user, with Apache, when combined with [suEXEC](https://httpd.apache.org/docs/2.4/suexec.html), CGI scripts can by run as specified users within one chroot, see the [users role](https://git.coop/webarch/users), the [Apache](https://git.coop/webarch/apache), [PHP](https://git.coop/webarch/php) and [SSH](https://git.coop/webarch/ssh) roles.

## rsyslog

The solution for `syslog` in the chroot was taken from [this Stack Exchange](https://serverfault.com/a/878659) comment.

## mini_sendmail

To enable mail to be sent using `/usr/sbin/sendmail` within the chroot a [fork of mini_sendmail](https://github.com/webarch-coop/mini_sendmail) is installed when `chroot_mini_sendmail` is True.

## loopback device

To create a new disk image to be used for `/chroot`:

```bash
fallocate -l 6G chroot.img
mkfs.ext4 chroot.img
```

Or:

```bash
qemu-img create -f raw chroot.img 6G
mkfs.ext4 chroot.img
```

## Copyright

Copyright 2019-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
